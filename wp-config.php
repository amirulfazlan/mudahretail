<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mudahborongretail');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Y$}h6t&[%!Fh^~zm4*-%?3zEs,~Z)JNm^hni6TDxD5B1]Vbc$y_~5n$q1LC1+I}K');
define('SECURE_AUTH_KEY',  'u/v@UMJ.dJ>J[|L/@Y~%@a_1/5f$Co^!p#_RS`/3$$Phc+Vf#|Xc{97_WBY`op^@');
define('LOGGED_IN_KEY',    '}/iw520txbL^i(oS1%6}m$D|KgqanM ]t&9cI02b35}Cxb|bO`o|Bf#g|m3us[;j');
define('NONCE_KEY',        'Wt<63Y5Y,!VEcQtGrj=vd[!U-sA62>2Y7d;NB8@h)`xe*Cg0<{x-E_(Ue76,E&R~');
define('AUTH_SALT',        '*QAcW3nVunZD3::(XDjRZ9RF&t#1g,ksvn>MH,2(|]t8N*v s69Ud6*QHopk>6+d');
define('SECURE_AUTH_SALT', 'Q|oP9CTfu!03)uXp+e3=`&n]o-+hA=@_+l}d*fz=p6%D!uJ##E&cV]P6-=?N4|eh');
define('LOGGED_IN_SALT',   '8PF$/EDK>O~}gRSPfq#gjtzD+bO>pU2g=et/9MR<1qP82%7H*`pux[K]2/E%1Wcz');
define('NONCE_SALT',       'VPdir1>{mB9`Q1qa[&]a@--JuS=3RL{.)mB6R`(u,*eCh-;Lg9w_$AAP#4eTC[#S');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'rt_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
